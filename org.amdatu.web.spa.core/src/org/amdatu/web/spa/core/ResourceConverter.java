package org.amdatu.web.spa.core;

import java.io.InputStream;
import java.io.OutputStream;

public interface ResourceConverter {
	void convertResource(String origPath, InputStream is, OutputStream os);
	String getNewPath(String origPath);
}
