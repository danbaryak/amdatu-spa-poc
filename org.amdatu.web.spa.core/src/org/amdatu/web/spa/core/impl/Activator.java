package org.amdatu.web.spa.core.impl;

import java.util.Properties;

import javax.servlet.Servlet;

import org.amdatu.web.spa.core.ResourceConverter;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {
		
	}

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		
		Properties props = new Properties();
		props.put("alias", "/");
		manager.add(createComponent()
				.setInterface(Servlet.class.getName(), props)
				.setImplementation(WebResourceServlet.class)
				.add(createServiceDependency()
						.setService(WebResourceStore.class)
						.setRequired(true)));
		
		manager.add(createComponent()
				.setImplementation(WebResourceHandler.class)
				.add(createServiceDependency()
						.setService(WebResourceStore.class)
						.setRequired(true))
				.add(createServiceDependency()
						.setService(ResourceConverter.class)
						.setCallbacks("resourceConverterAdded", "resourceConverterRemoved"))
				.add(createServiceDependency()
						.setService(LogService.class)
						.setRequired(true))
				.add(createBundleDependency()
						.setFilter("(X-SPA-Include=*)")
						.setCallbacks("bundleAdded", "bundleChanged", "bundleRemoved")));
		
		
		manager.add(createComponent()
				.setInterface(WebResourceStore.class.getName(), null)
				.setImplementation(WebResourceStoreImpl.class));
				
	}
	
}
