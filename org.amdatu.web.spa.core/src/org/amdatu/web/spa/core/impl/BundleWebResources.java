package org.amdatu.web.spa.core.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BundleWebResources {
	
	private long m_bundleId;
	private Map<String, WebResource> m_resources;
	
	public BundleWebResources(long bundleId) {
		m_bundleId = bundleId;
		m_resources = new ConcurrentHashMap<>();
	}
	
	public WebResource getResource(String path) {
		return m_resources.get(path);
	}
	
	public void addResource(WebResource resource) {
		m_resources.put(resource.getOrigPath(), resource);
	}
	
	public List<WebResource> getAllResources() {
		return new ArrayList<>(m_resources.values());
	}
}
