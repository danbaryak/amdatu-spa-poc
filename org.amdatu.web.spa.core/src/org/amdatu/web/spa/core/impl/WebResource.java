package org.amdatu.web.spa.core.impl;

/**
 * Describes a web resource, such as a script, image or any other file.
 */
public class WebResource {
	private String m_origPath;
	private long m_bundleId;
	private String m_localPath;
	private long m_lastModified;
	
	public String getOrigPath() {
		return m_origPath;
	}
	
	public void setOrigPath(String origPath) {
		m_origPath = origPath;
	}
	
	public void setBundleId(long bundleId) {
		m_bundleId = bundleId;
	}
	
	public long getBundleId() {
		return m_bundleId;
	}
	
	public String getLocalPath() {
		return m_localPath;
	}
	
	public void setLocalPath(String localPath) {
		m_localPath = localPath;
	}
	
	public long getLastModified() {
		return m_lastModified;
	}
	
	public void setLastModified(long lastModified) {
		m_lastModified = lastModified;
	}
	
}
