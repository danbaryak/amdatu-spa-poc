package org.amdatu.web.spa.core.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.web.spa.core.ResourceConverter;
import org.apache.commons.io.IOUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

public class WebResourceHandler {
	
	private volatile LogService logService;
	private volatile WebResourceStore m_store;
	private volatile BundleContext context;
	
	private Map<ServiceReference, ResourceConverter> m_resourceConverters;
	
	public WebResourceHandler() {
		m_resourceConverters = new ConcurrentHashMap<>();
	}
	
	public void start() {
		
	}
	
	public void bundleAdded(Bundle bundle) {
		
		String resourceDirsVal = String.valueOf(bundle.getHeaders().get("X-SPA-Include"));
		if (resourceDirsVal != null) {
			String[] resourceDirs = resourceDirsVal.split(",");
			
			BundleWebResources bundleResources = m_store.getBundleResources(bundle.getBundleId());
			
			for (String resourceDir : resourceDirs) {
				logService.log(LogService.LOG_INFO, "Processing all resources in " + resourceDir);
				Enumeration<?> entries = bundle.findEntries("/" + resourceDir, "*", true);
				Enumeration entryPaths = bundle.getEntryPaths("/");
				while (entryPaths.hasMoreElements()) {
					Object path = entryPaths.nextElement();
					System.out.println("ENTRY PATH: " + path);
				}
				while (entries.hasMoreElements()) {
					URL resourceUrl = (URL) entries.nextElement();
					String origPath = resourceUrl.getPath();
					origPath = origPath.substring(origPath.indexOf(resourceDir) + resourceDir.length());
					if (origPath.endsWith("/")) {
						continue;
					}
					try {
						// copy the resource to the local bundle cache
						InputStream is = resourceUrl.openStream();
						
						ResourceConverter converter = getConverterFor(origPath);
						
						String localPath = origPath;
						if (converter != null) {
							localPath = converter.getNewPath(origPath);
						}
						
						File localFile = m_store.getDataFile(localPath);
						localFile.getParentFile().mkdirs();
						FileOutputStream fos = new FileOutputStream(localFile);
						
						if (converter != null) {
							// apply conversion
							converter.convertResource(origPath, is, fos);
						} else {
							// just copy the file
							IOUtils.copy(is, fos);
							fos.flush();
							fos.close();
						}
						
						WebResource resource = new WebResource();
						resource.setBundleId(bundle.getBundleId());
						resource.setOrigPath(origPath);
						resource.setLocalPath(localPath);
						resource.setLastModified(System.currentTimeMillis());
						
						bundleResources.addResource(resource);
					} catch (Exception ex) {
						logService.log(LogService.LOG_ERROR, "Error while handling resource", ex);
					}
					
					logService.log(LogService.LOG_INFO, "entry: " + origPath);
					
				}
			}
		}
	}
	
	private ResourceConverter getConverterFor(String path) {
		String extension = path.substring(path.lastIndexOf(".") + 1);
		for (Entry<ServiceReference, ResourceConverter> entry : m_resourceConverters.entrySet()) {
			ServiceReference ref = entry.getKey();
			String handledExt = (String) ref.getProperty("extension");
			if (handledExt != null && handledExt.equals(extension)) {
				return entry.getValue();
			}
		}
		return null;
	}
	
	public void resourceConverterAdded(ServiceReference ref, ResourceConverter converter) throws IOException {
		
		String handledExtension = (String) ref.getProperty("extension");
		if (handledExtension == null) {
			logService.log(LogService.LOG_ERROR, "Resource converter does not declare handled extension");
			return;
		}
		
		List<WebResource> resources = m_store.getAllResources();
		for (WebResource res : resources) {
			if (res.getOrigPath().endsWith("." + handledExtension)) {
				String newPath = converter.getNewPath(res.getOrigPath());
				File localFile = m_store.getDataFile(newPath);
				localFile.getParentFile().mkdirs();
				FileOutputStream fos = new FileOutputStream(localFile);
				Bundle bundle = context.getBundle(res.getBundleId());
				URL resourceUrl = bundle.getResource("/www" + res.getOrigPath());
				InputStream is = resourceUrl.openStream();
				converter.convertResource(res.getOrigPath(), is, fos);
				res.setLocalPath(newPath);
			}
		}
		
		m_resourceConverters.put(ref, converter);
	}
	
	public void resourceConverterRemoved(ServiceReference ref) {
		m_resourceConverters.remove(ref);
	}
	
	public void bundleChanged(Bundle bundle) {
		System.out.println("Bundle " + bundle.getBundleId() + " changed");
	}
	
	public void bundleRemoved(Bundle bundle) {
		System.out.println("Bundle " + bundle.getBundleId() + " removed");
	}
	public void bundleSwapped(Bundle bundle) {
		System.out.println("Bundle " + bundle.getBundleId() + " swapped");
	}
}
