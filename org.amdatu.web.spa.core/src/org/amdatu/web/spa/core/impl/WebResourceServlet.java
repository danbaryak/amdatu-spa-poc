package org.amdatu.web.spa.core.impl;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebResourceServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private volatile WebResourceStore m_store;

	public void start() {

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String path = request.getPathInfo();
		if ("/".equals(path)) {
			// serve main page
		} else {
			WebResource resource = m_store.getResource(path);
			if (resource == null) {

				return;
			}
			File file = m_store.getDataFile(resource.getLocalPath());
			response.setContentType(determineContentType(file.getAbsolutePath()));
			response.setCharacterEncoding(Charset.defaultCharset().name());
			// resource requested
			m_store.streamResource(path, response.getOutputStream());
		}
	}

	private String determineContentType(String resourceName) {
		String mimetype = getServletContext().getMimeType(resourceName);
		if (mimetype == null) {
			mimetype = "application/octet-stream";

			if (resourceName.endsWith(".json")) {
				mimetype = "application/json";
			}
		}
		return mimetype;
	}
}
