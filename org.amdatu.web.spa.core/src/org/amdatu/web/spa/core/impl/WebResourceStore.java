package org.amdatu.web.spa.core.impl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;


public interface WebResourceStore {
	public WebResource getResource(String originalPath);
	public BundleWebResources getBundleResources(long bundleId);
	public File getDataFile(String path);
	boolean streamResource(String path, OutputStream os) throws IOException;
	public List<WebResource> getAllResources();
}
