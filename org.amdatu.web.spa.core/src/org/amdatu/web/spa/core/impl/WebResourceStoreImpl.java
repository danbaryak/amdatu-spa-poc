package org.amdatu.web.spa.core.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;
import org.osgi.framework.BundleContext;

public class WebResourceStoreImpl implements WebResourceStore {

	
	private Map<Long, BundleWebResources> m_bundleResources;
	private volatile BundleContext context;
	
	public WebResourceStoreImpl() {
		m_bundleResources = new ConcurrentHashMap<>(); 
	}
	
	@Override
	public WebResource getResource(String originalPath) {
		WebResource resource = null;
		for (BundleWebResources resources : m_bundleResources.values()) {
			resource = resources.getResource(originalPath);
			if (resource != null) {
				break;
			}
		}
		return resource;
	}
	
	@Override
	public BundleWebResources getBundleResources(long bundleId) {
		synchronized (m_bundleResources) {
			BundleWebResources resources = m_bundleResources.get(bundleId);
			if (resources == null) {
				resources = new BundleWebResources(bundleId);
				m_bundleResources.put(bundleId, resources);
			}
			return resources;
		}
	}
	
	@Override
	public File getDataFile(String path) {
		if (path.startsWith("/")) {
			path = path.substring(1);
		}
		return context.getDataFile(path);
	}
	
	@Override
	public boolean streamResource(String path, OutputStream os) throws IOException {
		WebResource resource = getResource(path);
		if (resource == null) {
			return false;
		}
		String localPath = resource.getLocalPath();
		if (localPath.startsWith("/")) {
			localPath = localPath.substring(1);
		}
		File file = context.getDataFile(localPath);
		
		IOUtils.copy(new FileInputStream(file), os);
		return true;
	}
	
	@Override
	public List<WebResource> getAllResources() {
		List<WebResource> resources = new ArrayList<>();
		for (BundleWebResources bundleRes : m_bundleResources.values()) {
			resources.addAll(bundleRes.getAllResources());
		}
		return resources;
	}
}
