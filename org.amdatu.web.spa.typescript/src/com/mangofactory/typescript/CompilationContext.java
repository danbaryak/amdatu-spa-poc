package com.mangofactory.typescript;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeObject;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class CompilationContext {

	private  final String name;
	
	private final File basePath;
	
	
	public CompilationContext(String name, File basePath) {
		this.name = name;
		this.basePath = basePath;
	}
	
	public String getName() {
		return this.name;
	}
	
	public File getBasePath() {
		return this.basePath;
	}
	
	private final Map<String, NativeObject> generatedFileReferences = Maps.newHashMap();
	private boolean throwExceptionOnCompilationFailure = true;
	
	private final List<TypescriptCompilationProblem> problems = Lists.newArrayList();
	
	public void addError(NativeObject error)
	{
		problems.add(TypescriptCompilationProblem.fromNativeObject(error));
	}

	public Integer getErrorCount() {
		return problems.size();
	}
	
	public void throwIfCompilationFailed()
	{
		if (!throwExceptionOnCompilationFailure)
			return;
		if (problems.isEmpty())
			return;
		throw new TypescriptException(problems);
		
	}
	public boolean getThrowExceptionOnCompilationFailure()
	{
		return throwExceptionOnCompilationFailure;
	}

	public NativeArray resolveFiles(NativeArray referencedFiles) throws Exception
	{
		List<NativeObject> codeUnits = Lists.newArrayList();
		for (int i = 0; i < referencedFiles.size(); i++)
		{
			NativeObject referencedFile = (NativeObject) referencedFiles.get(i);
			String path = (String) referencedFile.get("path");
			String fullPath = FilenameUtils.concat(basePath.getCanonicalPath(), path);
//			if (generatedFileReferences.containsKey(fullPath))
////			{
//				continue;
//			}
			
			String source = FileUtils.readFileToString(new File(fullPath));
			
			NativeObject codeUnit = new NativeObject();
			NativeObject.putProperty(codeUnit, "content", source);
			NativeObject.putProperty(codeUnit, "path", path);
			codeUnits.add(codeUnit);
			generatedFileReferences.put(fullPath, codeUnit);
		}
		NativeObject[] nativeObjects = codeUnits.toArray(new NativeObject[]{});
		return new NativeArray(nativeObjects);
	}
	
	public TypescriptCompilationProblem getProblem(int i) {
		return null;
	}

	public boolean hasProblems() {
		return !problems.isEmpty();
	}
}
