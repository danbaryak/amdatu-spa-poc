package com.mangofactory.typescript;


public enum EcmaScriptVersion {

	ES3("TypeScript.LanguageVersion.EcmaScript3"),
	ES5("TypeScript.LanguageVersion.EcmaScript5");

	private String js;
	
	private EcmaScriptVersion(String js)
	{
		this.js = js;
	}
	
	public String getJs() {
		return this.js;
	}
}
