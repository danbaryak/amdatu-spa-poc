package org.amdatu.web.spa.typescript;

import java.util.Properties;

import org.amdatu.web.spa.core.ResourceConverter;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {
		
	}

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		Properties properties = new Properties();
		properties.put("extension", "ts");
		manager.add(createComponent()
				.setInterface(ResourceConverter.class.getName(), properties)
				.setImplementation(TypescriptResourceHandler.class));
	}

}
