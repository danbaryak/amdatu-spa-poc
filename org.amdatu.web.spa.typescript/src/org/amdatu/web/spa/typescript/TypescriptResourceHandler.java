package org.amdatu.web.spa.typescript;

import java.io.InputStream;
import java.io.OutputStream;

import org.amdatu.web.spa.core.ResourceConverter;
import org.apache.commons.io.IOUtils;

import com.mangofactory.typescript.TypescriptCompiler;

public class TypescriptResourceHandler implements ResourceConverter {

	private TypescriptCompiler compiler;
	
	public void start() {
		compiler = new TypescriptCompiler();
	}
	
	@Override
	public void convertResource(String origPath, InputStream is,
			OutputStream os) {
		try {
			String inputStr = IOUtils.toString(is);
			String outputStr = compiler.compile(inputStr);
			IOUtils.write(outputStr, os);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public String getNewPath(String origPath) {
		if (origPath.endsWith(".ts")) {
			return origPath.substring(0, origPath.lastIndexOf(".")) + ".js";
		}
		return origPath;
	}
}
