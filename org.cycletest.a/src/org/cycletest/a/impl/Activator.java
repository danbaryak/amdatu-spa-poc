package org.cycletest.a.impl;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.cycletest.a.ServiceA;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
	
	private volatile LogService logService;
	private volatile ServiceA serviceA;
	
	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		
	}

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		
		manager.add(createComponent()
				.setInterface(ServiceA.class.getName(), null)
				.setImplementation(ServiceAImpl.class)
				.add(createServiceDependency().setService(LogService.class)));
//				.add(createServiceDependency().setService(ServiceB.class).setRequired(true)));
		
		Properties properties = new Properties();
		properties.put("osgi.command.scope", "cycle");
		properties.put("osgi.command.function", "sayHello");
		manager.add(createComponent().setInterface(Object.class.getName(), properties).setImplementation(this)
				.add(createServiceDependency().setService(LogService.class))
				.add(createServiceDependency().setService(ServiceA.class).setRequired(true)));
		
		
	}
	
	public String sayHello() {
		logService.log(LogService.LOG_INFO, "sayHello() called");
		return serviceA.sayHello();
	}
}
