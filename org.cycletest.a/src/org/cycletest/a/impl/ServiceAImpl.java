package org.cycletest.a.impl;

import org.cycletest.a.ServiceA;
import org.osgi.service.log.LogService;

public class ServiceAImpl implements ServiceA {
	
	private volatile LogService logService;
	
	@Override
	public String sayHello() {
		logService.log(LogService.LOG_DEBUG, "Message from ServiceA");
		return "Hello from A";
	}
}
