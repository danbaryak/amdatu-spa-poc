package org.cycletest.b.impl;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.cycletest.b.ServiceB;
import org.osgi.framework.BundleContext;


public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		
	}

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		
		manager.add(createComponent()
				.setInterface(ServiceB.class.getName(), null)
				.setImplementation(ServiceBImpl.class));
		
	}

}
